import Controls from './src/controls.js'

$(document).ready(function(){
	const controls = new Controls()
	controls.init()
});
