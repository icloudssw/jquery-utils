const webpack = require("webpack")
const path = require("path")
const WatchTimePlugin = require("webpack-watch-time-plugin")
const glob = require("glob")

module.exports = {
	entry: {
        "jquery-utils": glob.sync("./index.js"),
	},

	stats: {
		errorDetails: true,
		colors: true,
		modules: true,
		reasons: true
	},

	output: {
		path: path.resolve("../../webroot"),
		filename: "js/[name].js",
		sourceMapFilename: "js/[name].js.map"
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
				}
			}
		]
	},

	plugins: [
		new WatchTimePlugin({
			noChanges: {
				detect: true,
				report: true
			},
			logLevel: "warn"
		})
	]
}
