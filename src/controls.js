var $element;

export default class Controls {


	constructor(){
		$('[add-item-btn]').fadeIn();
		$('[save-item-btn]').hide()
		$('[cancel-item-btn]').hide();


		this.watch()
	}

	watch(){
		this.delete()
		this.submit()
		this.edit()
	}

	init(){
		/**
		* Other instructions
		*
		* - Add no-clean-identifier attribute on inputs that you not clean no way
		* - Optional Add Jquery validate on specific page
		*/
		$('#form-validate-items').on('submit', function(events){
			if($('#form-validate-items').valid()){
				events.preventDefault();
				$.ajax({
					type: 'POST',
					url: $($element).attr('add-item-url'),
					data: $('#form-validate-items').serialize(),
					statusCode: {
						400: function () {
							swal({
								title: "Desculpe",
								text: "Algo deu errado ao salvar, tente novamente mais tarde. Se o problema persistir entre em contato com o suporte.",
								icon: "error",
								type: "error",
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "Entendi",
							}, function(){
								location.reload()
							});
						},
						500: function () {
							swal({
								title: "Desculpe",
								text: "Algo deu errado ao salvar, tente novamente mais tarde. Se o problema persistir entre em contato com o suporte.",
								icon: "error",
								type: "error",
								confirmButtonColor: "#DD6B55",
								confirmButtonText: "Entendi",
							}, function(){
								location.reload()
							});
						},
						200: function (response) {
							$('[add-item-context]').prepend(response);
							$('th').click(()=>location.reload()) // because i do not used datatables function to add rows in table, but was used the prepend method
							$('[add-item-context]').hide();
							$('[add-item-context]').fadeIn();
							new Controls() // Rebind all
							toastr.options.preventDuplicates = true;
							toastr.success('Sincronizado com sucesso!')
						}
					},
				})

				$('[add-item-btn]').fadeIn();
				$('[save-item-btn]').hide()
				$('[cancel-item-btn]').hide();
			}
		});

		this.cancel()
	}

	/**
	* @description Delete a item and remove all occurrences in html
	* For example, delete a item of page and remove of dom without reload page
	*
	* @author Gabriel Dissotti
	*
	* How to use:
	*
	* 1. Add attribute delete-item-action="<?= $id_do_elemento ?>"
	* 2. Add attribute delete-item-url="<?= $url_para_delete ?>" into same element of delete-item-action (to get the id element)
	* 3. Add attribute delete-item-element="<?= $id_do_elemento ?>" on element that you want remove from DOM if request is success
	*
	*/
	delete(){

		$('[delete-item-action]').unbind();
		$('[delete-item-action]').click(function(){
			var $element = $(this);
			var itemId = $($element).attr('delete-item-action');

			swal({
				title: "Você tem certeza?",
				text: "Deseja realmente apagar este item?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Confirmar",
				cancelButtonText: "Cancelar",
				closeOnConfirm: false,
				showLoaderOnConfirm: true,
			}, function () {
				$.ajax({
					url: $($element).attr('delete-item-url') + '?id=' + itemId,
					statusCode: {
						400: function () {
							swal("Erro!", "Desculpe, algo deu errado!", "error");
						},
						200: function () {
							$('[delete-item-element="'+itemId+'"').hide();
							$('[delete-item-element="'+itemId+'"').remove();
							swal("Apagado!", "", "success");
						}
					},
				})
			});
		})
	}

	/**
	* @description add a item and add in a context.
	* For example, create an user and add on table without reload page
	*
	* @author Gabriel Dissotti
	*
	* How to use:
	*
	* 1. Add attribute add-item-action="#formToSubmit"
	* 2. Add attribute add-item-url="<?= $url_para_add ?>" into same element of add-item-action
	* 	  > This url can answer a html to append in add-item-context
	* 3. Add attribute add-item-context on element that you want add item into them
	* 4. Add form-validate-items id on form element
	*
	*/
	submit(){
		$('[add-item-action]').unbind();
		$('[add-item-action]').click(function(){
			$element = $(this);
			$('[temporally-hidden]').remove();

			$('#form-validate-items').submit()
		})
		this.clean()
	}

	/**
	* @deprecated DOCUMENTATION
	* @description Edit item
	* For example, edit the fields of an user without reload the page
	*
	* @author Gabriel Dissotti
	*
	* How to use:
	*
	* 1. add the attribute edit-item-action="#itemId" in a element that you want click to edit
	* 2. add the edit-item-reference="#itemId" attribute in all elements to the js distinct values of items
	* 3. add the edit-item-input="#idOfnput" attribute in all elements to get the values from them
	* 4. if not exists yet, add the edit-item-context on element  that you want add item into them
	* 5. add edit-item-cancel attribute on a button that you want use to cancel alterations (clean input id item)
	* 6. if not exists yet, add add-item-action attribute on element to save updates
	* 7. Add edit-item-identifier attribute in the input hidden of your formulary
	* 8. Add attributes: add-item-btn save-item-btn and cancel-item-btn in respective buttons to hidden or show buttons
	*/
	edit(){
		this.clean()
		$('[edit-item-action]').unbind();
		$('[edit-item-action]').click(function(){
			window.scrollTo(0, 0);
			//blink
			$("[controls-blink-form-edit]").css("box-shadow","1px 1px 30px white");//define opacidade inicial
			var interval = setInterval(function() {
				if($("[controls-blink-form-edit]").css("box-shadow") == 'none'){
					$("[controls-blink-form-edit]").css("box-shadow","1px 1px 30px white");
				}else{
					$("[controls-blink-form-edit]").css("box-shadow","none");
					clearInterval(interval)
				}
			}, 300);


			var $element = $(this);
			var idItem = $($element).attr('edit-item-action');
			var url = $($element).attr('edit-item-action-url');

			$('[add-item-btn]').hide();

			$('[delete-item-element="'+idItem+'"]').attr('temporally-hidden',true);

			$('[save-item-btn]').fadeIn()
			$('[cancel-item-btn]').fadeIn();

			$.ajax({
				url: `${url}${idItem}`
			}).done((competitor) => {
				let res = JSON.parse(competitor)
				res.map((field) => {
					$(`[data-item-field="${field.key}"]`).val(field.value)
					$(`[data-item-field="${field.key}"]`).trigger('change')
				})
			})


			/* $('[delete-item-element="'+idItem+'"] [edit-item-reference="'+idItem+'"],[delete-item-element="'+idItem+'"] [edit-item-input]').each(function(i, elem) {
				var inputName = $(elem).attr('edit-item-input');
				$('input[name="'+inputName+'"]').val($(elem).html().trim())

				var optionByText = $('option:contains("'+$(elem).html().trim()+'")').val();
				if(optionByText != undefined){
					$('select[name="'+inputName+'"]').val(optionByText);
				}

				$('[edit-item-identifier]').val(idItem)
			}); */
		})
	}

	cancel(){
		$('[edit-item-cancel]').click(() => {
			this.clean()
		});
	}

	/**
	* add form-items-reset attribute on form tag
	*/
	clean(){
		$('[temporally-hidden]').removeAttr('temporally-hidden');

		$('[edit-item-identifier]').val('');
		$('[form-items-reset] input').not( "[no-clean-identifier]" ).val('')
		$('[form-items-reset] select').not('[no-clean]').val('');
		$('[form-items-reset] select').not('[no-clean]').trigger('change');

		$('[add-item-btn]').fadeIn();
		$('[save-item-btn]').hide()
		$('[cancel-item-btn]').hide();
	}
}
